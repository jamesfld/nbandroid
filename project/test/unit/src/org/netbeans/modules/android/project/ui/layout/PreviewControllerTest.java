package org.netbeans.modules.android.project.ui.layout;

import com.android.ide.common.resources.configuration.FolderConfiguration;
import com.android.resources.Density;
import com.android.resources.Keyboard;
import com.android.resources.KeyboardState;
import com.android.resources.Navigation;
import com.android.resources.NavigationState;
import com.android.resources.ScreenOrientation;
import com.android.resources.ScreenRatio;
import com.android.resources.ScreenSize;
import com.android.resources.TouchScreen;
import com.android.resources.UiMode;
import com.android.sdklib.devices.ButtonType;
import com.android.sdklib.devices.Device;
import com.android.sdklib.devices.Device.Builder;
import com.android.sdklib.devices.DeviceManager;
import com.android.sdklib.devices.Hardware;
import com.android.sdklib.devices.Multitouch;
import com.android.sdklib.devices.PowerType;
import com.android.sdklib.devices.Screen;
import com.android.sdklib.devices.ScreenType;
import com.android.sdklib.devices.Software;
import com.android.sdklib.devices.State;
import com.android.sdklib.devices.Storage;
import com.android.sdklib.devices.Storage.Unit;
import org.junit.AfterClass;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Mockito.*;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.openide.filesystems.FileObject;

/**
 *
 * @author radim
 */
public class PreviewControllerTest {
  
  private static AndroidTestFixture fixture;

  private static FileObject snakePrj;
  private static FileObject someSource1;
  private static FileObject someLayout1;
  private static FileObject someOtherRes;
  
  private static FileObject playerPrj;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake").
        withProject("RandomMusicPlayer", "samples/android-17/RandomMusicPlayer");
    snakePrj = fixture.getProjectFolder("Snake");
    playerPrj = fixture.getProjectFolder("RandomMusicPlayer");

    someSource1 = snakePrj.getFileObject("src/com/example/android/snake/Snake.java");
    someLayout1 = snakePrj.getFileObject("res/layout/snake_layout.xml");
    someOtherRes = snakePrj.getFileObject("res/values/strings.xml");
  }


  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  @Test
  public void testUpdateCurrentFileObject() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);
    // make sure null is harmless
    ctrl.updateFileObject(null);
    
    ctrl.updateFileObject(someSource1);
    assertNotSame(someSource1, model.getFileObject());
    ctrl.updateFileObject(someLayout1);
    assertEquals(someLayout1, model.getFileObject());
    // only updated when layout is selected
    ctrl.updateFileObject(someOtherRes);
    assertEquals(someLayout1, model.getFileObject());
  }
  
  @Test
  public void testFileObjectSetsPlatform() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);
    
    final FileObject layoutFile = playerPrj.getFileObject("res/layout/main.xml");
    ctrl.updateFileObject(layoutFile);
    
    assertEquals(layoutFile, model.getFileObject());
    assertEquals("platform is set based on AndroidManifest.xml",
        DalvikPlatformManager.getDefault().findPlatformForTarget("android-14"), model.getPlatform());
  }
  
  @Test
  public void fileObjectPlatformIsKeptWhenSufficient() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);
    
    ctrl.updatePlatform(DalvikPlatformManager.getDefault().findPlatformForTarget("android-17"));
    final FileObject layoutFile = playerPrj.getFileObject("res/layout/main.xml");
    ctrl.updateFileObject(layoutFile);
    
    assertEquals(layoutFile, model.getFileObject());
    assertEquals("previously set platform is still valid",
        DalvikPlatformManager.getDefault().findPlatformForTarget("android-17"), model.getPlatform());
  }
  
  @Test
  public void testFileObjectWillGetSomeDeviceConfig() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);
    
    final FileObject layoutFile = playerPrj.getFileObject("res/layout/main.xml");
    ctrl.updateFileObject(layoutFile);
    
    assertEquals(layoutFile, model.getFileObject());
    assertNotNull(model.getDevice());
    assertNotNull(model.getDeviceConfig());
  }
  
  @Test
  public void selectThemeForLayout() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);
    
    final FileObject layoutFile = playerPrj.getFileObject("res/layout/main.xml");
    ctrl.updateFileObject(layoutFile);
    
    assertEquals(layoutFile, model.getFileObject());
    assertNotNull(model.getTheme());
    // TODO should be Holo
    assertEquals("Theme", model.getTheme().themeName);
  }
  
  private Device testConfig() {
    DeviceManager mgr = DalvikPlatformManager.getDefault().getDeviceManager();
    Builder builder = new Device.Builder();
    
    Software sw = new Software();
    sw.setGlVersion("4.2");
    sw.setLiveWallpaperSupport(false);
    sw.setMaxSdkLevel(42);
    sw.setMinSdkLevel(1);
    sw.setStatusBar(true);

    Screen sc = new Screen();
    sc.setDiagonalLength(7);
    sc.setMechanism(TouchScreen.FINGER);
    sc.setMultitouch(Multitouch.JAZZ_HANDS);
    sc.setPixelDensity(Density.HIGH);
    sc.setRatio(ScreenRatio.NOTLONG);
    sc.setScreenType(ScreenType.CAPACITIVE);
    sc.setSize(ScreenSize.LARGE);
    sc.setXDimension(5);
    sc.setXdpi(100);
    sc.setYDimension(4);
    sc.setYdpi(100);

    Hardware hw = new Hardware();
    hw.setButtonType(ButtonType.SOFT);
    hw.setChargeType(PowerType.BATTERY);
    hw.setCpu("armeabi-v7a");
    hw.setGpu("pixelpushing");
    hw.setHasMic(true);
    hw.setKeyboard(Keyboard.QWERTY);
    hw.setNav(Navigation.NONAV);
    hw.setRam(new Storage(512, Unit.MiB));
    hw.setScreen(sc);

    builder.addSoftware(new Software());
    builder.setName("testDevice");
    builder.setManufacturer("testManufacturer");
    State config = new State();
    config.setName("testConfig");
    config.setDescription("Portrait");
    config.setDefaultState(true);
    config.setKeyState(KeyboardState.SOFT);
    config.setNavState(NavigationState.HIDDEN);
    config.setHardware(hw);
    config.setOrientation(ScreenOrientation.PORTRAIT);
    
    builder.addSoftware(sw);
    builder.addState(config);
    Device testDevice = builder.build();
    mgr.addUserDevice(testDevice);

    return testDevice;
  }
  
  public void testFolderConfig() {
    Device testDevice = testConfig();
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);

    ctrl.updateFileObject(someLayout1);
    ctrl.updatePlatform(DalvikPlatformManager.getDefault().findPlatformForTarget("android-8"));
    reset(repainter);
    
    ArgumentCaptor<PreviewModel> modelCaptor = ArgumentCaptor.forClass(PreviewModel.class);
    ArgumentCaptor<FolderConfiguration> configCaptor = ArgumentCaptor.forClass(FolderConfiguration.class);
    ctrl.updateConfiguration(testDevice.getState("testConfig"));
    
    verify(repainter).paint(modelCaptor.capture(), configCaptor.capture());
//    System.err.println("fo " + foCaptor.getValue());
//    System.err.println("platform " + platformCaptor.getValue());
//    System.err.println("config " + configCaptor.getValue());
    assertEquals(UiMode.NORMAL, configCaptor.getValue().getUiModeQualifier().getValue());
    reset(repainter);
    
    ctrl.updateUiMode(UiMode.DESK);
    verify(repainter).paint(modelCaptor.capture(), configCaptor.capture());
    assertEquals(UiMode.DESK, configCaptor.getValue().getUiModeQualifier().getValue());
  }

  @Test
  public void switchToLandscape() {
    PreviewController.Repainter repainter = mock(PreviewController.Repainter.class);
    PreviewModel model = new PreviewModel();
    PreviewController ctrl = new PreviewController(model, DalvikPlatformManager.getDefault(), repainter);

    ctrl.updatePlatform(DalvikPlatformManager.getDefault().findPlatformForTarget("android-17"));
    ctrl.updateConfiguration(testConfig().getState("testConfig"));
    reset(repainter);
    
    ArgumentCaptor<PreviewModel> modelCaptor = ArgumentCaptor.forClass(PreviewModel.class);
    ArgumentCaptor<FolderConfiguration> configCaptor = ArgumentCaptor.forClass(FolderConfiguration.class);
    
    ctrl.updateFileObject(playerPrj.getFileObject("res/layout/main.xml"));
    verify(repainter).paint(modelCaptor.capture(), configCaptor.capture());
    // assertEquals(ScreenOrientation.PORTRAIT, configCaptor.getValue().getScreenOrientationQualifier().getValue());
    
    // TODO(radim): why it this not null now?
    // assertNull(configCaptor.getValue().getDensityQualifier());
    
    reset(repainter);
    ctrl.updateFileObject(playerPrj.getFileObject("res/layout-land/main.xml"));
    verify(repainter).paint(modelCaptor.capture(), configCaptor.capture());
    assertEquals(ScreenOrientation.LANDSCAPE, configCaptor.getValue().getScreenOrientationQualifier().getValue());
    // assertNull(configCaptor.getValue().getDensityQualifier());
  }
}
