/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.project;

import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import static org.junit.Assert.*;
import org.junit.Test;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.project.api.AndroidProjects;

/**
 * Tests for AndroidActionProvider
 */
public class AndroidProjectUtilTest  {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");
  private static final String DUMMY_SDK_DIR = "/some/path/to/android-sdk-linux_x86";

  @Test
  public void parseManifest() {
    assertNull(AndroidProjects.parseProjectManifest((Project) null));
  }
  
  @Test
  public void toPlatform() {
    DalvikPlatformManager dpm = DalvikPlatformManager.getDefault();
    // run w/o SDK to check if we can init it
    assertNull(dpm.getSdkLocation());

    DalvikPlatform platform = AndroidProjectUtil.toDalvikPlatorm(
        "file:" + SDK_DIR + "/platforms/android-8/");
    assertNotNull(platform);
    assertEquals(SDK_DIR, dpm.getSdkLocation());

    // another try with URL that uses same SDK
    platform = AndroidProjectUtil.toDalvikPlatorm(
        "file:" + SDK_DIR + "/platforms/android-8/");
    assertNotNull(platform);
    assertEquals(SDK_DIR, dpm.getSdkLocation());
    // another try with URL that uses different SDK should fail
    platform = AndroidProjectUtil.toDalvikPlatorm(
        "file:/another/path/android-sdk-linux_x86/platforms/android-8/");
    assertNull(platform);
    assertEquals(SDK_DIR, dpm.getSdkLocation());
  }
}
