/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint;

import com.android.tools.lint.LintCliXmlParser;
import com.android.tools.lint.LombokParser;
import com.android.tools.lint.client.api.Configuration;
import com.android.tools.lint.client.api.DefaultConfiguration;
import com.android.tools.lint.client.api.IDomParser;
import com.android.tools.lint.client.api.IJavaParser;
import com.android.tools.lint.client.api.LintClient;
import com.android.tools.lint.client.api.LintDriver;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Location;
import com.android.tools.lint.detector.api.Project;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.ast.Node;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.modules.android.project.api.AndroidProjects;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Union2;

/**
 *
 * @author lahoda@gmail.com
 */
public class RunLint {

    private static final Logger LOG = Logger.getLogger("org.netbeans.modules.android.lint");
    private static final long MAX_JAVA_PARSING = 10000;
    private final AtomicReference<LintDriver> currentDriver = new AtomicReference<LintDriver>();
    private final AtomicBoolean cancel = new AtomicBoolean();
    
    public List<ErrorDescription> analyze(Iterable<? extends Union2<FileObject, NonRecursiveFolder>> toAnalyze) {
        cancel.set(false);
        
        final List<ErrorDescription> errors = new ArrayList<ErrorDescription>();
        final Set<File> todo = new HashSet<File>();

        for (Union2<FileObject, NonRecursiveFolder> a : toAnalyze) {
            if (a.hasFirst()) {
                recursiveSearch(a.first(), todo);
            } else {
                for (FileObject c : a.second().getFolder().getChildren()) {
                    if (!isSupported(c)) continue;
                    File f = FileUtil.toFile(c);
                    if (f == null) continue;
                    todo.add(f);
                }
            }
        }

        final File sdkRoot;

        if (!todo.isEmpty()) {
            FileObject f = FileUtil.toFileObject(todo.iterator().next());

            if (f != null) {
                sdkRoot = AndroidProjects.sdkOf(FileOwnerQuery.getOwner(f));
            } else {
                sdkRoot = null;
            }
            } else {
                sdkRoot = null;
        }
        
        LintDriver l = new LintDriver(Registry.getLintRegistry(), new LintClient() {
            @Override public void report(Context cntxt, Issue issue, com.android.tools.lint.detector.api.Severity svrt, Location lctn, String string, Object o) {
                if (!todo.contains(lctn.getFile())) {
                    //why this happens?
                    return ;
                }

                Severity severity;
                
                switch (issue.getDefaultSeverity()) {
                    case ERROR: severity = Severity.ERROR; break;
                    case WARNING: severity = Severity.VERIFIER; break;
                    case INFORMATIONAL: severity = Severity.VERIFIER; break; //TODO: ???
                    default:
                    case IGNORE: return;
                }

                FileObject file = FileUtil.toFileObject(lctn.getFile());

                if (file == null) return ;

                if (lctn.getStart() == null) {
                    LOG.log(Level.WARNING, "no start position location for: {0}, location: {1}", new Object[] {svrt.toString(), lctn.toString()});
                } else {
                    errors.add(ErrorDescriptionFactory.createErrorDescription(severity, string, file, lctn.getStart().getOffset(), lctn.getEnd().getOffset()));
                }
            }

            @Override public void log(Throwable thrwbl, String string, Object... os) {
                LOG.log(Level.INFO, String.format(string, os), thrwbl);
            }

            @Override public void log(com.android.tools.lint.detector.api.Severity svrt, Throwable thrwbl, String string, Object... os) {
                LOG.log(Level.INFO, String.format(string, os), thrwbl);
            }

            @Override public IDomParser getDomParser() {
                return new LintCliXmlParser();
            }

            @Override public String readFile(File file) {
                try {
                    return FileUtil.toFileObject(file).asText();
                } catch (IOException ex) {
                    throw new IllegalStateException();
                }
            }

            @Override public Configuration getConfiguration(Project project) {
                return new DefaultConfiguration(this, project, null) {
                    @Override public boolean isEnabled(Issue issue) {
                        return Registry.isEnabled(issue.getId());
                    }
                    
                };
            }

            @Override public IJavaParser getJavaParser() {
                //The lombok-ast parser used by Lint does not handle unparseable
                //source code very well, and sometimes goes into an infinite or
                //virtually infinite loop. Which then would render the IDE unusable.
                //So running the parser in its own thread, setting-up a timeout for it
                //and killing the thread when the time is up or when the processing
                //has been canceled.
                class CancellableLombokParser extends LombokParser {
                    @Override public Node parseJava(final JavaContext context) {
                        long start = System.currentTimeMillis();
                        final Node[] result = new Node[1];

                        Thread worker = new Thread() {
                            public void run() {
                                result[0] = CancellableLombokParser.super.parseJava(context);
                            }
                        };

                        worker.start();

                        boolean notTooLong = true;

                        while (worker.isAlive() && !cancel.get() && (notTooLong = System.currentTimeMillis() - start < MAX_JAVA_PARSING)) {
                            try {
                                worker.join(10);
                            } catch (InterruptedException ex) {
                                LOG.log(Level.FINE, null, ex);
                            }
                        }

                        LOG.log(Level.INFO, "Java parsing took: {0}", System.currentTimeMillis() - start);

                        if (cancel.get()) {
                            LOG.log(Level.INFO, "Hard cancel on the Java parser");
                            worker.stop();
                            return null;
                        }

                        if (!notTooLong) {
                            LOG.log(Level.INFO, "Stopping Java parser because it took too long.");
                            worker.stop();
                            return null;
                        }

                        return result[0];
                    }
                };

                return new CancellableLombokParser();
            }

            @Override
            public File findResource(String relativePath) {
                if (sdkRoot != null) {
                    File result = new File(sdkRoot, relativePath);

                    if (result.canRead()) return result;
                    return null;
                }
                return null;
            }

        });

        currentDriver.set(l);

        try {
            l.analyze(new ArrayList<File>(todo), Registry.EDITOR_SCOPES);
        } finally {
            currentDriver.set(null);
        }

        return errors;
    }

    private static final String[] ACCEPTED_MIME_TYPES = new String[] {
        "text/x-java",
        "text/xml",
    };

    private static final Set<String> ACCEPTED_MIME_TYPES_SET = new HashSet<String>(Arrays.asList(ACCEPTED_MIME_TYPES));

    private static void recursiveSearch(FileObject from, Set<File> into) {
        if (isSupported(from)) {
            File f = FileUtil.toFile(from);

            if (f != null) {
                into.add(f);
            }
        }

        for (FileObject c : from.getChildren()) {
            recursiveSearch(c, into);
        }
    }

    private static boolean isSupported(FileObject from) {
        return ACCEPTED_MIME_TYPES_SET.contains(FileUtil.getMIMEType(from, ACCEPTED_MIME_TYPES));
    }
    
    public void cancel() {
        LintDriver l = currentDriver.get();

        if (l != null) {
            l.cancel();
        }

        cancel.set(true);
    }

}
