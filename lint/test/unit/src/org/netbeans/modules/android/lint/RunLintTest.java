/*
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *  under the License.
 */
package org.netbeans.modules.android.lint;

import com.android.tools.lint.detector.api.Issue;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.fileinfo.NonRecursiveFolder;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidGeneralData;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.mimelookup.MimeDataProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.Union2;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author lahoda@gmail.com
 */
public class RunLintTest {
    
  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  private static File tempFolder;
  private static FileObject projdir;

  private static Project pp;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices(MimeLookupProviderImpl.class);
    DalvikPlatformManager.getDefault().setSdkLocation(SDK_DIR);
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();

    FileObject scratch = FileUtil.toFileObject(tempFolder);
    FileObject sdkDirFo = FileUtil.toFileObject(new File(SDK_DIR));

    projdir = scratch.createFolder("Test");
    FileUtilities.recursiveCopy(sdkDirFo.getFileObject("samples/android-8/SkeletonApp"), projdir);

    pp = ProjectManager.getDefault().findProject(projdir);
    FileUtil.setMIMEType("xml", "text/xml");
    
    for (Issue i : Registry.getLintRegistry().getIssues()) {
        Registry.setEnabled(i.getId(), false);
    }
  }


  @AfterClass
  public static void delete() throws BackingStoreException {
    FileUtilities.recursiveDelete(tempFolder);
    NbPreferences.forModule(Registry.class).removeNode();
  }

  @Test
  public void errorForXML() throws Exception {
      performErrorTest("res/layout/skeleton_activity.xml",
                       "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                       "<LinearLayout xmlns:android=\"http://schemas.android.com/apk/res/android\"\n" +
                       "    android:orientation=\"vertical\"\n" +
                       "    android:layout_width=\"fill_parent\"\n" +
                       "    android:layout_height=\"fill_parent\"\n" +
                       "    >\n" +
                       "<TextView  \n" +
                       "    android:id=\"@+id/editor\"\n" +
                       "    android:layout_width=\"fill_parent\" \n" +
                       "    android:layout_height=\"wrap_content\" \n" +
                       "    android:text=\"Hello World, MainActivity\"\n" +
                       "    android:textColor=\"#FFFFFF\"\n" +
                       "    />\n" +
                       "</LinearLayout>",
                       "HardcodedText",
                       "10:4-10:44:verifier:[I18N] Hardcoded string \"Hello World, MainActivity\", should use @string resource");
  }
  
  @Test
  public void errorForManifest() throws Exception {
      performErrorTest("AndroidManifest.xml",
                       "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                       "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\"\n" +
                       "      package=\"test.test\"\n" +
                       "      android:versionCode=\"1\"\n" +
                       "      android:versionName=\"1.0\">\n" +
                       "    <application android:label=\"@string/app_name\" >\n" +
                       "        <activity android:name=\"MainActivity\"\n" +
                       "                  android:label=\"@string/app_name\">\n" +
                       "            <intent-filter>\n" +
                       "                <action android:name=\"android.intent.action.MAIN\" />\n" +
                       "                <category android:name=\"android.intent.category.LAUNCHER\" />\n" +
                       "            </intent-filter>\n" +
                       "        </activity>\n" +
                       "    </application>\n" +
                       "    <uses-sdk android:minSdkVersion=\"8\" />\n" +
                       "</manifest>\n",
                       "ManifestOrder",
                       "14:4-14:42:verifier:<uses-sdk> tag appears after <application> tag");
  }

  @Test
  public void errorForJavaSource() throws Exception {
      FileUtil.createData(projdir, "src/com/example/android/skeletonapp/Test.java");
      performErrorTest("src/com/example/android/skeletonapp/Test.java",
                       "package com.example.android.skeletonapp;\n" +
                       "public class Test {\n" +
                       "    private final java.util.HashMap<Integer, String> map = new java.util.HashMap<Integer, String>();\n" +
                       "}\n",
                       "UseSparseArrays",
                       "2:59-2:99:verifier:Use new SparseArray<String>(...) instead for better performance");
  }
  
  private void performErrorTest(String fileName, String content, String enable, String... golden) throws Exception {
    Registry.setEnabled(enable, true);
    final AndroidProject proj = (AndroidProject) ProjectManager.getDefault().findProject(projdir);
    AndroidGeneralData data = AndroidGeneralData.fromProject(proj);
    data.setPlatform(DalvikPlatformManager.getDefault().findPlatformForTarget("android-8"));
    proj.update(data);
    FileObject testFile = projdir.getFileObject(fileName);
    assertNotNull(testFile);

    TestUtilities.copyString2File(testFile, content);
    
    final Set<String> results = new HashSet<String>();
    
    for (ErrorDescription ed : new RunLint().analyze(Collections.singletonList(Union2.<FileObject, NonRecursiveFolder>createFirst(testFile)))) {
        results.add(ed.toString());
    }
    
    assertEquals(new HashSet<String>(Arrays.asList(golden)), results);
  }
  
  public static final class MimeLookupProviderImpl implements MimeDataProvider {

      private static final Lookup XML_PARSER = Lookups.fixed(new XMLParser.FactoryImpl());
      
        @Override
        public Lookup getLookup(MimePath mimePath) {
            if ("text/xml".equals(mimePath.getPath())) {
                return XML_PARSER;
            }
            
            return null;
        }
      
  }
}
