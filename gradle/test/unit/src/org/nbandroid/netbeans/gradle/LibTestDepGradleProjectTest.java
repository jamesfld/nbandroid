package org.nbandroid.netbeans.gradle;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;

import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.junit.Assert.*;
import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class LibTestDepGradleProjectTest {
  private static GradleProjectFixture prjFix;
  private static FileObject foProjectSrc;
  private static PluginsFixture pluginsFix;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("libTestDep")
        .useIs(GradleTests.testArchiveStream())
        .create();
    
    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void basic() throws Exception {
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.COMPILE, 
        "guava-11.0.2.jar"
        );
    verifyClasspath(prjFix, foProjectSrc, ClassPath.BOOT, "android.jar");
    
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }
}