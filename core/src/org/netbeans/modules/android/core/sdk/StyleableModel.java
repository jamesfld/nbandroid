/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.netbeans.modules.android.core.sdk;

import java.util.SortedMap;

/**
 * A wrapper around XML files describing structure of Android XML files.
 *
 * @author Radim Kubacki
 */
public interface StyleableModel {

  AttributeInfo getAttributeValue(String attrName);

  SortedMap<String, StyleableInfo> getStyleables();
}
