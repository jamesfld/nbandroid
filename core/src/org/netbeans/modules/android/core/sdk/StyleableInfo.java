/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import java.util.List;
import java.util.Set;

/**
 * Object that can be included in Android XML file to describe view or view layout element.
 *
 * @author radim
 */
public interface StyleableInfo {
  String getName();

  String getDescription();

  /** Names of parent tags that can contain this styleable. Applicable to tag styleables. */
  Set<String> getParentNames();

  List<AttributeInfo> getAttributeNames();
}
