/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import org.nbandroid.netbeans.test.TestPlatform;
import com.google.common.collect.Lists;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class DalvikPlatformTest {

  private static final String SDK_DIR = System.getProperty("test.all.android.sdks.home");

  private DalvikPlatformManager dpm;

  @Before
  public void setUp() {
    dpm = new DalvikPlatformManager();
    dpm.setSdkLocation(SDK_DIR);
  }

  @Test
  public void icecreamThemes() throws Exception {
    TestPlatform p = TestPlatform.ANDROID_4_0_3;

    DalvikPlatform platform = dpm.findPlatformForTarget(p.target);
    List<String> themes = Lists.newArrayList(platform.getThemes());
    
    assertNotNull(themes);
    for (String themeName : new String[] {"Theme", "Theme.Holo.CompactMenu"}) {
      assertTrue(themeName, themes.contains(themeName));
    }
    assertFalse(themes.contains("Widget.Holo.Light"));
  }
}
